#!/bin/sh -xe

TOOLSDIR=${TOOLSDIR:-../tools}

if test ! armv7.img -nt releasedir/evbarm-earmv7hf/binary/gzimg/armv7.img.gz; then
    gunzip < releasedir/evbarm-earmv7hf/binary/gzimg/armv7.img.gz > armv7.img.tmp
    mv armv7.img.tmp armv7.img
fi

url=http://ftp.netbsd.org//pub/NetBSD/arch/evbarm/odroid-c1/u-boot-odroidc-v2011.03-20220520.tar.gz
test -r $(basename $url) || wget $url
test -r u-boot.bin || gunzip < $(basename $url) | tar xpvf -

url=http://ftp.netbsd.org/pub/NetBSD/misc/jmcneill/odroidc1/u-boot-odroidc-v2011.03-20150308.tar.gz
test -r $(basename $url) || wget $url
test -r u-boot.bin || gunzip < $(basename $url) | tar xpvf -

cp armv7.img odroid-c1.img
dd  if=bl1.bin.hardkernel   of=odroid-c1.img bs=1    count=442                conv=notrunc
dd  if=bl1.bin.hardkernel   of=odroid-c1.img bs=512            skip=1 seek=1  conv=notrunc
dd  if=u-boot.bin           of=odroid-c1.img bs=512                   seek=64 conv=notrunc
