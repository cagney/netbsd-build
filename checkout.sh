#!/bin/sh

set -e

# parse the arguments
if test $# -ne 1 ; then
    cat <<EOF 1>&2
Usage: $0 [ <branch> ]
EOF
    exit 1
fi

branch=$1

# git or cvs?
case ${branch} in
    *.git )
	checkout=git
	branch=$(basename ${branch} .git)
	;;
    *.cvs )
	checkout=cvs
	branch=$(basename ${branch} .cvs)
	;;
    *.hg )
	checkout=hg
	branch=$(basename ${branch} .hg)
	;;
    * )
	checkout=hg
	;;
esac

# rev?
case ${branch} in
    [1-9]* ) rev="netbsd-${branch}" ;;
    trunk|trunc|main|mainline ) rev="" ;;
    *-* ) rev="${branch}" ;;
    * )
	echo "Unknown revision ${branch}" 1>&2
	exit 1
	;;
esac

# create top level
echo mkdir -p ${branch}

# ...and checkout
for d in src xsrc ; do
    case ${checkout} in
	git )
	    opts=$(if test -n "${rev}" ; then echo -b ${rev} ; fi)
	    echo git clone ${opts} https://github.com/netbsd/${d} ${branch}/${d}
	    ;;
	hg )
	    opts=$(if test -n "${rev}" ; then echo -b ${rev} ; fi)
	    echo hg clone ${opts} https://anonhg.NetBSD.org/${d} ${branch}/${d}
	    ;;
	cvs )
	    opts=$(if test -n "${rev}" ; then echo -r ${rev} ; fi)
	    echo CVS_RSH=ssh cvs -f -q -d anoncvs@anoncvs.netbsd.org/cvsroot checkout ${opts} -P -d ${branch}/${d} ${d}
	    ;;
    esac
done
