#!/bin/sh

set -e

#export HOST_CC='cc -fcommon'
#export HOST_CXX='c++ -fcommon'
export HOST_CFLAGS='-fcommon -Wno-error -Wno-literal-suffix -Wno-expansion-to-defined -Wno-implicit-fallthrough'
export HOST_CXXFLAGS='-fcommon -Wno-error -Wno-literal-suffix -Wno-expansion-to-defined -Wno-implicit-fallthrough -std=c++11'

print() {
    printf '  %-10s %s\n' "$1" "$2"
}
echo

machine="-m $(basename $(pwd))"
print Machine: "$machine"

objdir="-O $(pwd)"
print Objdir: "$objdir"

toolsdir="-T $(dirname $(pwd))/tools"
print Toolsdir: "$toolsdir"

if test -d ../xsrc ; then
    x11dir="-x -X $(cd ../xsrc && pwd)"
else
    x11dir=
fi
print X11: "$x11dir"

# Use heuristic to specify -j (jobs).
jobs=
if test -r /proc/cpuinfo ; then
    jobs=$(awk 'BEGIN {cores=0 ; processors=0} /cpu cores/ {cores=$4} /processor/ {processors += 1} END {print "-j", cores + 1}' /proc/cpuinfo)
else
    jobs=$(cpuctl list | grep online | wc -l | sed -e '/[0-9]/ s/^/-j /')
fi
print Jobs: "$jobs"
jobs=

echo

cd ../src
exec nice nice ./build.sh \
     ${machine} \
     -u \
     -U \
     ${jobs} \
     ${x11dir} \
     ${objdir} \
     ${toolsdir} \
     "$@"
