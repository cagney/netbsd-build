#!/bin/sh -ex

case $(uname -s) in
    NetBSD) ;;
    *) echo "unknown os" 1>&2
       exit 1
       ;;
esac

processor=$(uname -p)
case $(basename $(pwd)) in
    *${processor}* ) ;;
    *) echo "non-native build" 1>&2
       exit 1
       ;;
esac

machine=$(uname -m)
kernel=$(uname -v | sed -e 's;.*/;;')
if test ! -f ../src/sys/arch/${machine}/conf/${kernel} ; then
    echo "unknown kernel ${KERNEL}; assuming GENERIC"
    kernel=GENERIC
fi

../../build.sh tools kernel=${kernel}
