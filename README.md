Overview
--------

Some simple scripts to ease the pain of multiple
netbsd builds from a single source tree.


Layout
------

These scripts create a layout like:

    7/src/        NetBSD 7 sources
    7/xsrc/       NetBSD 7 xsources
    7/<machine>   NetBSD 7 <machine> build directory
    7/tools/      NetBSD 7 cross compile tools


First time
----------

To set up the directory "9" containing NetBSD 9 run:

    ./checkout.sh 9.cvs | sh
    ./checkout.sh 9.git | sh
    ./checkout.sh 9 | sh		# defaults to CVS

(Or "./update.sh trunk" to get trunk).

To find a machine (or alias) run:

    7/src/build.sh list-arch

for instance, for the odroid-c1, evbearmv7hf-el is a good fit:

    mkdir 7/evbearmv7hf-el


Everytime
---------

To update the tree, and then build for odroid-c1 (evbearmv7hf-el) run:

    cd 9/evbearmv7hf-el
    ../../update.sh | sh
    ../../release.sh
    ../../odroid-c1-9.sh

will do something aproaching the right thing.


TODO
----

Integrating PKGSRC would be nice.
