#!/bin/sh -e

# parse the arguments
if test $# -gt 1 ; then
    cat <<EOF
Usage: $0 [<directory>]
EOF
    exit 1
fi

if test $# -eq 1 ; then
    p=$1/
elif test -d src ; then
    p=
elif test -d ../src ; then
    p=../
else
    echo "I'm lost" 1>&2
    exit 1
fi

for d in src xsrc ; do
    # path
    # git or cvs ?
    cd=${p}${d}
    if test ! -d ${cd} ; then
	continue
    fi
    # invoke correct program
    if test -d ${cd}/.git ; then
	echo "( cd ${cd} && git fetch && git rebase )"
    elif test -d ${cd}/CVS ; then
	echo "( cd ${cd} && cvs -f -q update -d -P )"
    elif test -d ${cd}/.hg ; then
	echo "( cd ${cd} && hg pull && hg update )"
    else
	echo "I'm confused" 1>&1
	exit 1
    fi
done
